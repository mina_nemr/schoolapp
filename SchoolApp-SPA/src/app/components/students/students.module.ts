import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxButtonModule } from 'devextreme-angular';

import { StudentsRoutingModule } from './students-routing.module';
import { StudentsComponent } from '../../modules/student/pages/student-list/student-list.component';
import { StudentService } from '../../core/services/student.service';

@NgModule({
  imports: [
    CommonModule,
    DxDataGridModule,
    DxButtonModule,
    StudentsRoutingModule
  ],
  declarations: [StudentsComponent],
  providers: [
    StudentService
  ]
})
export class StudentsModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { OdataService } from './odata.service';
import { Student } from '../models/student/student.model';
import { StudentForm } from '../models/student/studentForm.model';
import ODataStore from 'devextreme/data/odata/store';
import { config } from '../app-config';


@Injectable({ providedIn: 'root' })
export class StudentService {
    private studentsStore: ODataStore;
    private studentForm: FormData;

    constructor(private httpClient: HttpClient, private oDataService: OdataService) {
        this.studentsStore = oDataService.configureStore('Id', 'students');
    }

    getStudents() {
        const student = new Student();
        return {
            store: this.studentsStore,
            select: Object.keys(student)
        };
    }
    getStudent(key: number) {
        return this.studentsStore.byKey(key);
    }

    add(newStudent: StudentForm) {
        this.studentForm = new FormData();

        Object.keys(newStudent).forEach((key) => {
            this.studentForm.append(key, newStudent[key]);
        });
        return this.httpClient.post<void>(config.API_URL + 'students', this.studentForm, {
            observe: 'body',
            responseType: 'json'
        });
    }

    update(newStudent: StudentForm, studentId: number) {
        this.studentForm = new FormData();
        console.log();
        // tslint:disable-next-line: max-line-length
        const date = newStudent.BirthDate.getFullYear() + '-' + ("0" + (newStudent.BirthDate.getMonth() + 1)).slice(-2) + '-' + ("0" + newStudent.BirthDate.getDate()).slice(-2);
        Object.keys(newStudent).forEach((key) => {
            if (key === 'BirthDate') {
                this.studentForm.append(key, date);
            } else {
                this.studentForm.append(key, newStudent[key]);
            }

        });
        return this.httpClient.put<void>(config.API_URL + 'students/' + studentId, this.studentForm);
    }

    delete(studentId: number) {
        return this.studentsStore.remove(studentId);
    }
}

import { Injectable } from '@angular/core';
import { config } from '../app-config';
import ODataStore from 'devextreme/data/odata/store';

@Injectable({
    providedIn: 'root'
})

export class OdataService {

    constructor() { }

    configureStore = (key: string, endpoint: string): ODataStore => {
        return new ODataStore({
            key,
            url: config.API_URL + endpoint,
            version: 4,
            keyType: 'Int32',
        })
    }

    delete(store: ODataStore, key: any) {
        store.remove(key)
    }
}

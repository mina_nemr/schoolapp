export class StudentForm {
    Id: number;
    Name: string;
    Country: string;
    City: string;
    Gender: string;
    BirthDate: Date;
    Image: File;
}
export class Student {
    Id: number;
    Name: string;
    Country: string;
    City: string;
    Gender: string;
    BirthDate: string;
    ImageUrl: string;
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
    DxDataGridModule,
    DxButtonModule, DxSelectBoxModule,
    DxTextAreaModule,
    DxDateBoxModule,
    DxFormModule,
    DxAutocompleteModule,
    DxFileUploaderModule,
    DxValidatorModule,
    DxLoadPanelModule
} from 'devextreme-angular';

import { StudentRoutingModule } from './student.routing';
import { StudentsComponent } from './pages/student-list/student-list.component';
import { StudentComponent } from './pages/student-form/student-form.component';
import { StudentService } from '../../core/services/student.service';

@NgModule({
    imports: [
        CommonModule,
        StudentRoutingModule,
        DxDataGridModule,
        DxButtonModule,
        DxSelectBoxModule,
        DxTextAreaModule,
        DxDateBoxModule,
        DxAutocompleteModule,
        DxFormModule,
        DxFileUploaderModule,
        DxValidatorModule,
        DxLoadPanelModule
    ],
    declarations: [StudentsComponent, StudentComponent],
    providers: [
        StudentService
    ]
})
export class StudentModule { }

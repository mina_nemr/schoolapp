import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsComponent } from './pages/student-list/student-list.component';
import { StudentComponent } from './pages/student-form/student-form.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: '',
        children: [
            {
                path: 'list',
                component: StudentsComponent
            },
            {
                path: 'add',
                component: StudentComponent
            },
            {
                path: 'edit',
                component: StudentComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StudentRoutingModule { }

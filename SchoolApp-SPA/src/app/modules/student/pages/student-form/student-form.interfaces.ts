import { Student } from '../../../../core/models/student/student.model';
export interface StudentFormSettings {
    studentId?: number;
    student?: Student;
    isEdit?: boolean;
    isPosting?: boolean;
    avatarUrl?: string;
}

export interface StudentFormRules {
    namePattern?: RegExp;
    maxDate?: Date;
    minDate?: Date;
}

export interface StudentFormOptions {
    gender?: any[];
    city?: any[];
    country?: any[];
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
  StudentService
} from '../../../../core/services/student.service';
import { StudentForm } from '../../../../core/models/student/studentForm.model';
import * as countriesJSON from '../../../../shared/data/countries.json';
import { StudentFormOptions, StudentFormRules, StudentFormSettings } from './student-form.interfaces';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-student',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.scss']
})

export class StudentComponent implements OnInit {

  studentForm: StudentForm = new StudentForm();
  formSettings: StudentFormSettings;
  formRules: StudentFormRules;
  formOptions: StudentFormOptions;
  formFileUploader: File[] = [];
  countriesJSON: any = countriesJSON;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private studentService: StudentService) {
    const appTime = new Date();
    this.formSettings = {
      isEdit: false,
      avatarUrl: '../../../../../assets/avatar.jpg',
      isPosting: false
    };
    this.formOptions = {
      gender: ['Male', 'Female'],
      city: [],
      country: Object.keys(this.countriesJSON.default),
    };
    this.formRules = {
      namePattern: /^[^0-9]+$/,
      maxDate: new Date(appTime.getFullYear() - 5, appTime.getMonth(), appTime.getDate()),
      minDate: new Date(appTime.getFullYear() - 90, appTime.getMonth(), appTime.getDate())
    };
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params.id !== undefined) {
        this.formSettings.studentId = params.id;
        this.formSettings.isEdit = true;
        this.getStudent(this.formSettings.studentId);
      }
    });
  }

  getStudent(id: number) {
    this.studentService.getStudent(id).then(
      (data) => {
        this.formSettings.student = data;
        Object.keys(this.formSettings.student).forEach((key) => {
          if (key === 'Country') {
            this.formOptions.city = this.countriesJSON.default[this.studentForm.Country];
            this.studentForm[key] = this.formSettings.student[key];

          } else if (key === 'ImageUrl' && this.formSettings.student[key] !== null) {
            this.formSettings.avatarUrl = this.formSettings.student.ImageUrl;
          } else if (key === 'BirthDate' && this.formSettings.student[key] !== null) {
            this.studentForm[key] = new Date(this.formSettings.student[key]);
          } else {
            this.studentForm[key] = this.formSettings.student[key];
          }
        });
        // this.studentForm.City = this.formSettings.student['City'];
      },
      () => {
        notify('Something went wrong!!', 'warning');
      }
    );
  }

  createStudent() {
    this.studentService.add(this.studentForm)
      .subscribe(() => {
        this.formSettings.isPosting = false;
        notify(this.studentForm.Name + ' was added successfully', 'success');
        this.router.navigate(['student/list']);
      }, () => {
        this.formSettings.isPosting = false;
        notify('Something went wrong!!', 'warning');
      }
      );
  }

  updateStudent() {
    this.studentService.update(this.studentForm, this.formSettings.studentId)
      .subscribe(() => {
        this.formSettings.isPosting = false;
        notify(this.studentForm.Name + ' was editted successfully', 'success');
        this.router.navigate(['student/list']);
      }, () => {
        this.formSettings.isPosting = false;
        notify('Something went wrong!!', 'warning');
      }
      );
  }

  showPreviewImage() {
    this.studentForm.Image = this.formFileUploader[0];
    if (this.studentForm.Image) {
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.formSettings.avatarUrl = event.target.result;
      };
      reader.readAsDataURL(this.studentForm.Image);
    }
  }

  onCountryChange = (obj: { previousValue: any; }) => {
    this.formOptions.city = this.countriesJSON.default[this.studentForm.Country];
    if (obj.previousValue !== undefined) {
      this.studentForm.City = this.formOptions.city[0];
    }
  }


  submitForm(e: { preventDefault: () => void; }) {
    e.preventDefault();
    this.formSettings.isPosting = true;
    if (this.formSettings.studentId !== undefined) {
      this.updateStudent();
    } else {
      this.createStudent();
    }
  }


}

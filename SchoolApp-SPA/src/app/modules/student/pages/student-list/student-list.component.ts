import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../../../../core/services/student.service';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';

@Component({
    selector: 'app-student-list',
    templateUrl: './student-list.component.html',
    styleUrls: ['./student-list.component.scss']
})
export class StudentsComponent implements OnInit {
    studentDataSource: any;
    @ViewChild('studentsGrid') studentsGrid: DxDataGridComponent;

    constructor(private router: Router, private studentService: StudentService) { }

    ngOnInit() {
        this.studentDataSource =
            this.studentService.getStudents();
    }

    editStudent(id: number) {
        this.router.navigate(['student/edit'], { queryParams: { id } });
    }

    addStudent() {
        this.router.navigate(['student/add']);
    }

    deleteStudent(id: number) {
        this.studentService.delete(id).then(
            () => {
                this.studentsGrid.instance.refresh();
                notify('The student was deleted successfully', 'error');
            },
            () => {
                notify('Something went wrong!!', 'warning');
            }
        );
    }
}

using Microsoft.AspNetCore.Http;

namespace SchoolApp.API.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Gender { get; set; }
        public string ImageUrl { get; set; }
        public string BirthDate { get; set; }
        public Student() { }
        public Student(PostStudentRequest student) : this()
        {
            this.Name = student.Name;
            this.Country = student.Country;
            this.City = student.City;
            this.BirthDate = student.BirthDate;
            this.Gender = student.Gender;
        }
        public Student(PostStudentRequest student, int studentId) : this(student)
        {
            this.Id = studentId;
        }
    }
    public class PostStudentRequest
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public IFormFile Image { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
    }
}
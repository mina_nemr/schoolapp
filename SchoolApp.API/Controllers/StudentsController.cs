﻿using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using SchoolApp.API.Data;
using SchoolApp.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

namespace SchoolApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ODataController
    {
        private readonly DataContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly string _uploadsPath;

        public StudentsController(IHostingEnvironment environment, DataContext context)
        {
            _environment = environment ?? throw new ArgumentNullException(nameof(environment));
            _context = context;
            _uploadsPath = Path.Combine(_environment.WebRootPath, "uploads");
        }

        [EnableQuery]
        public async Task<IActionResult> Get()
        {
            var allStudents = await _context.Students.ToListAsync();
            return Ok(allStudents);
        }
        public async Task<IActionResult> Get([FromODataUri]int key)
        {
            var storedStudent = await _context.Students.FirstOrDefaultAsync(student => student.Id == key);
            return Ok(storedStudent);
        }

        public async Task<IActionResult> Post([FromForm] PostStudentRequest studentData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Student = new Student(studentData);
            if (studentData.Image != null && studentData.Image.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(_uploadsPath, studentData.Image.FileName), FileMode.Create))
                {
                    await studentData.Image.CopyToAsync(fileStream);
                    Student.ImageUrl = Path.Combine("https://localhost:3001/uploads/", studentData.Image.FileName);
                }
            }
            _context.Students.Add(Student);
            await _context.SaveChangesAsync();
            return Created(Student);

        }
        public async Task<IActionResult> Put([FromODataUri]int key, [FromForm] PostStudentRequest studentData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var modifiedStudent = new Student(studentData, key);
            if (studentData.Image != null && studentData.Image.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(_uploadsPath, studentData.Image.FileName), FileMode.Create))
                {
                    await studentData.Image.CopyToAsync(fileStream);
                    modifiedStudent.ImageUrl = Path.Combine("https://localhost:3001/uploads/", studentData.Image.FileName);
                }
            }
            else
            {
                var originalStudent = await _context.Students.FindAsync(key);
                modifiedStudent.ImageUrl = originalStudent.ImageUrl;
                _context.Entry(originalStudent).State = EntityState.Detached;
            }
            _context.Entry(modifiedStudent).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(modifiedStudent);
        }





        public async Task<ActionResult> Delete([FromODataUri] int key)
        {
            var targetStudent = await _context.Students.FindAsync(key);
            if (targetStudent == null)
            {
                return NotFound();
            }
            _context.Students.Remove(targetStudent);
            await _context.SaveChangesAsync();
            return Ok("Deleted");
        }
        private bool StudentExists(int key)
        {
            return _context.Students.Any(student => student.Id == key);
        }
    }
}
